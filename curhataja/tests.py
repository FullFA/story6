from django.test import TestCase, Client
from django.urls import reverse, resolve
from django.utils import timezone

from .views import landing
from django.http.request import HttpRequest

from django.db import models
from .models import Curhat

from .forms import CurhatForm
import time

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options


# Create your tests here.

class Curhataja_Test(TestCase):
    def test_apakah_ada_url(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_jika_url_tidak_ada(self):
        response = Client().get('/url-tidak-ada')
        self.assertEqual(response.status_code, 404)

    def test_landing_template(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landing.html')

    def test_landing_function(self):
        found = resolve('/')
        self.assertEqual(found.func, landing)

    def test_ayo_curhat_aja(self):
        request = HttpRequest()
        response = landing(request)
        html_response = response.content.decode('utf8')
        self.assertIn('Ayo Curhat Aja', html_response)

    def test_model_bisa_buat_curhat_baru(self):
        
        curhatan_baru = Curhat.objects.create(tanggal=timezone.now(), curhatan='lagi galau')

        menghitung_jumlah_curhatan = Curhat.objects.all().count()
        self.assertEqual(menghitung_jumlah_curhatan,1)

    def test_save_POST_request(self):
        response = self.client.post('/', data={'curhatan' : 'Lagi Galau'})
        menghitung_jumlah_curhatan = Curhat.objects.all().count()
        self.assertEqual(menghitung_jumlah_curhatan, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

    def test_isi_form_valid(self):
        form_curhat = {'curhatan': 'Coba Coba'}
        form = CurhatForm(data=form_curhat)
        self.assertTrue(form.is_valid())

    def test_isi_form_invalid(self):
        form_curhat = {}
        form = CurhatForm(data=form_curhat)

        self.assertEquals(len(form.errors),1)


class story6FunctionalTest(TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')

        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(story6FunctionalTest, self).setUp()
    
    def tearDown(self):
        self.selenium.quit()
        super(story6FunctionalTest, self).tearDown()

    def test_input_curhat(self):
        selenium = self.selenium
        # OPEN THE LINK
        selenium.get('http://127.0.0.1:8000/')
        # FIND ELEMEN
        curhat = selenium.find_element_by_id('id_curhatan')
        submit = selenium.find_element_by_tag_name('button')
        # INPUT
        curhat.send_keys('Curhat dong Kak Pewe')
        time.sleep(3)
        submit.send_keys(Keys.RETURN)

    def test_ada_title(self):
        selenium = self.selenium
        selenium.get('http://127.0.0.1:8000/')
        header = selenium.find_element_by_tag_name('header');
        self.assertEqual(header.text, 'Ayo Curhat Aja');


