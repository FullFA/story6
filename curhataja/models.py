from django.db import models
from django.utils import timezone

# Create your models here.
class Curhat(models.Model):
    tanggal = models.DateTimeField(default=timezone.now)
    curhatan = models.TextField(max_length=300)
    