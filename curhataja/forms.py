from django import forms

from .models import Curhat

class CurhatForm(forms.ModelForm):
    class Meta:
        model = Curhat
        fields = [
            'curhatan',
        ]

        widgets = {
            'curhatan': forms.TextInput(
                attrs ={
                    'class':'form_control',
                    'placeholder':'Maksimal Curhatan 300 karakter ya :)',
                }
            ),

        }