from django.shortcuts import render, redirect

# Create your views here.
from .forms import CurhatForm
from .models import Curhat


def landing(request):
    curhatans = Curhat.objects.all()
    form_curhat = CurhatForm(request.POST or None)

    if request.method == 'POST':
        if form_curhat.is_valid():
            form_curhat.save()

            return redirect('landing')

    context = {
        'judul' : 'Ayo Curhat Aja',
        'curhat_form' : form_curhat,
        'curhatans':curhatans,
    }
    return render(request, 'landing.html', context)